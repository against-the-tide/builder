const { spawn, execSync } = require('child_process');
const { rmSync, readFileSync, writeFileSync, createWriteStream } = require('fs');
const path = require('path');

const archiver = require('archiver');
const commandLineArgs = require('command-line-args');
const semver = require('semver');
const yaml = require('yaml');

// -----------------------------------------------------------------------------

const options = commandLineArgs([
	{ name: 'minor', alias: 'm', type: Number, defaultValue: 0 },
	{ name: 'patch', alias: 'p', type: Number, defaultValue: 0 },
	{ name: 'deploy', alias: 'd', type: Boolean, defaultValue: false },
	{ name: 'no-windows', type: Boolean, defaultValue: false },
	{ name: 'no-linux', type: Boolean, defaultValue: false },
	{ name: 'no-osx', type: Boolean, defaultValue: false },
	{ name: 'web', type: Boolean, defaultValue: false },
	{ name: 'no-archive', type: Boolean, defaultValue: false },
	{ name: 'config', type: String },
	{ name: 'project', type: String },
	{ name: 'build', type: String },
	{ name: 'filename', type: String },
	{ name: 'archive', type: String },
	{ name: 'unity', type: String },
	{ name: 'unity-exe', type: String, defaultValue: 'unity.exe' },
	{ name: 'itch-user', type: String },
	{ name: 'itch-channel', type: String },
	{ name: 'env', type: String },
	{ name: 'env-source', type: String },
	{ name: 'env-out', type: String },
]);

// -----------------------------------------------------------------------------

process.env.YAML_SILENCE_WARNINGS = 1;

const projectSettingsPath = path.join(
	options.project || '',
	'ProjectSettings',
	'ProjectSettings.asset',
);

const bundleVersionRegex = /(\s*bundleVersion:\s*)(.*)(\r?\n)/i;

let originalProjectSettings;

// -----------------------------------------------------------------------------

(async () => {
	try {
		console.log('Unity Builder & Itch.io Deployer\n');

		console.log('Raw Flags:', options, '\n');

		console.log('Parsed Flags:');
		console.log('Environment (--env):', options.env);
		console.log('Environment Config (Source Folder, --env-source):', options['env-source']);
		console.log('Environment Config (Destination Folder, --env-out):', options['env-out']);
		console.log('Builder Config File (--config):', options.config);
		console.log('Project Folder (--project):', options.project);
		console.log('Build Folder (--build):', options.build);
		console.log('Unity Executable (--unity):', options.unity);
		console.log('Application Filename (--filename):', options.filename);
		console.log('Archive Name (--archive):', options.archive);
		console.log(`Deploy to Itch (--deploy): ${options.deploy}`);
		console.log('itch.io username (--itch-user):', options['itch-user']);
		console.log('itch.io channel (--itch-channel):', options['itch-channel']);
		console.log(`Increment Minor Version (--minor/--m): +${options.minor || 0}`);
		console.log(`Increment Path Version (--patch/--p): +${options.patch || 0}`);
		console.log(`Don't create archive: ${options['no-archive']}`);
		console.log(`Don't compile Windows: ${options['no-windows']}`);
		console.log(`Don't compile Linux: ${options['no-linux']}`);
		console.log(`Don't compile OSX: ${options['no-osx']}`);
		console.log(`Compile Web: ${options['web']}`);

		throwIf(!options.project, 'path to project folder must be set (--project)');
		throwIf(!options.build, 'path to build folder must be set (--build)');
		throwIf(!options.unity, 'path to unity executable must be set (--unity)');
		throwIf(!options.filename, 'application filename must be set (--filename)');
		throwIf(
			!options['no-archive'] && !options.archive,
			'archive filename must be set (--archive) without --no-archve',
		);
		throwIf(
			options.deploy && !options['itch-user'],
			'itch.io username must be set (--itch-user) with --deploy',
		);
		throwIf(
			options.deploy && !options['itch-channel'],
			'itch.io channel must be set (--itch-channel) with --deploy',
		);
		throwIf(options.minor < 0, "Minor version change (--minor/--m) can't be negative");
		throwIf(options.path < 0, "Patch version change (--patch/--p) can't be negative");
		throwIf(
			options.env && (!options['env-source'] || !options['env-out']),
			'--env-source and --env-out must be set with --env',
		);
		console.log(' ');

		console.log('Waiting 5 seconds...');
		await new Promise((resolve) => setTimeout(resolve, 5000));

		//
		setEnvironment();

		const applicationVersion = incrementApplicationVersion();
		const buildNumber = getBuildNumber();
		const finalApplicationVersion = buildNumber
			? `${applicationVersion}-${buildNumber}`
			: applicationVersion;
		console.log(`Final Application Version: ${finalApplicationVersion}`);

		const archives = await buildAndCompressExecutables(finalApplicationVersion);
		await deployPackagesToItchio(finalApplicationVersion, archives);

		console.log('Done!');
		process.exit();
	} catch (error) {
		console.log('');
		console.log(error);
		throw error;
	}
})();

// -----------------------------------------------------------------------------

function setEnvironment() {
	if (!options.env) {
		console.log('Skipping environment processing...');
		return;
	}

	//
	const filename = `environment.${options.env}.json`;
	const readPath = path.join(options['env-source'], filename);
	const writePath = path.join(options['env-out'], filename);
	let environment;

	try {
		console.log('Reading environment file:', readPath);
		environment = JSON.parse(readFileSync(readPath, { encoding: 'utf-8' }));
	} catch (error) {
		throw new Error(`Error reading environment file: ${readPath}\n${error}`);
	}

	try {
		console.log('Writing environment file:', writePath);
		writeFileSync(writePath, JSON.stringify(environment), { encoding: 'utf-8' });
	} catch (error) {
		throw new Error(`Error writing environment file: ${writePath}\n${error}`);
	}
}

function getBuildNumber() {
	try {
		if (!options.config) {
			console.log(`Build Number: (not set)`);
			return undefined;
		}

		//
		const unityBuilderContents = readFileSync(path.normalize(options.config), {
			encoding: 'utf-8',
		});
		const unityBuilder = JSON.parse(unityBuilderContents);

		const buildNumber = unityBuilder.build_number || 0;
		console.log(`Build Number: ${buildNumber}`);

		return buildNumber;
	} catch (error) {
		console.log('Failed to read builder config file!');
		throw error;
	}
}

function incrementApplicationVersion() {
	const projectSettingsContents = readFileSync(projectSettingsPath, { encoding: 'utf-8' });
	originalProjectSettings = projectSettingsContents;

	const projectSettings = yaml.parse(projectSettingsContents);

	//
	const bundleVersion = projectSettings.PlayerSettings.bundleVersion;

	let newBundleVersion = bundleVersion;
	if (options.minor > 0) {
		for (let i = 0; i < options.minor; i++) {
			newBundleVersion = semver.inc(newBundleVersion, 'minor');
		}
	}

	if (options.patch > 0) {
		for (let i = 0; i < options.patch; i++) {
			newBundleVersion = semver.inc(newBundleVersion, 'patch');
		}
	}

	//
	if (bundleVersion === newBundleVersion) {
		console.log(`Bundle Version: ${bundleVersion}`);
		return bundleVersion;
	}

	//
	console.log(`Bundle Version: ${bundleVersion} --> ${newBundleVersion}`);

	// unity does not use standard YAML practices (of course)
	// so we must do this old-school with regular expression
	const newProjectSettingsContents = projectSettingsContents.replace(
		bundleVersionRegex,
		`$1${newBundleVersion}$3`,
	);

	writeFileSync(projectSettingsPath, newProjectSettingsContents, { encoding: 'utf8' });

	//
	return newBundleVersion;
}

async function buildAndCompressExecutables(applicationNumber) {
	console.log('\nBuilding executables...');

	const builds = {};

	if (!options['no-windows']) {
		builds.windows = await buildAndCompressExecutable('windows', applicationNumber);
	}

	if (!options['no-osx']) {
		builds.osx = await buildAndCompressExecutable('osx', applicationNumber, 'unverified');
	}

	if (!options['no-linux']) {
		builds.linux = await buildAndCompressExecutable('linux', applicationNumber, 'unverified');
	}

	if (options['web']) {
		builds.web = await buildAndCompressExecutable('web', applicationNumber);
	}

	return builds;
}

async function buildAndCompressExecutable(platform, applicationNumber, suffix) {
	console.log(`\n${platform}...`);

	const buildPath = path.resolve(path.join(options.build, platform));
	const executablePath = path.resolve(getExecutablePath(buildPath, platform));

	console.log(`Build Folder: ${buildPath}`);
	console.log(`Executable Path: ${executablePath}`);

	const command = options['unity-exe'];
	const args = [
		'-quit',
		'-batchmode',
		'-logfile',
		`"${path.resolve(`./unity-builder-${platform}.log`)}"`,
		buildFlag(platform),
		`"${executablePath}"`,
	];
	console.log(`Build Command: ${path.join(options.unity, command)} ${args.join(' ')}`);

	//
	console.log('Removing build folder....');
	rmSync(buildPath, { recursive: true, force: true });

	//
	console.log('Building...');
	await new Promise((resolve, reject) => {
		spawn(command, args, {
			shell: true,
			cwd: options.unity.replace(/\\+/g, '\\\\'),
		}).on('close', (code) =>
			code == 0 ? resolve(null) : reject(new Error(`Unknown error code ${code}`)),
		);
	});

	//
	if (options['no-archive']) {
		return buildPath;
	}

	//
	const archivePath = path.resolve(
		path.join(
			options.build,
			`${options.archive}-${applicationNumber}-${platform}${suffix ? `-${suffix}` : ''}.zip`,
		),
	);

	console.log(`Archive Path: ${archivePath}`);
	console.log('Compressing...');
	await compressFolderToZip(buildPath, archivePath);

	return archivePath;
}

function buildFlag(platform) {
	switch (platform) {
		case 'windows':
			return '-buildWindows64Player';
		case 'osx':
			return '-buildOSXUniversalPlayer';
		case 'linux':
			return '-buildLinux64Player';
		case 'web':
			return '-executeMethod ATT.Build.Web.Executable';
	}
}

function getExecutablePath(buildPath, platform) {
	switch (platform) {
		case 'windows':
			return path.join(buildPath, `${options.filename}.exe`);
		case 'osx':
			return path.join(buildPath, `${options.filename}.app`);
		default:
			return path.join(buildPath, `${options.filename}`);
	}
}

function compressFolderToZip(folderToCompressPath, zipFilePath) {
	return new Promise((resolve, reject) => {
		const output = createWriteStream(zipFilePath);
		const archive = archiver('zip', {
			zlib: { level: 9 },
		});

		output.on('close', function () {
			// console.log(archive.pointer() + ' total bytes');
			// console.log('archiver has been finalized and the output file descriptor has closed.');
			resolve();
		});

		output.on('end', function () {
			// console.log('Data has been drained');
		});

		archive.on('warning', function (err) {
			if (err.code === 'ENOENT') {
				console.log('Warning:', err);
			} else {
				console.log('Error:', err);
				reject(err);
			}
		});

		archive.on('error', function (err) {
			console.log('Error:', err);
			reject(err);
		});

		archive.pipe(output);
		archive.directory(`${folderToCompressPath}\\`, false);
		archive.finalize();
	});
}

async function deployPackagesToItchio(version, archives = {}) {
	if (!options.deploy) {
		return;
	}

	//
	console.log('\nDeploying to itch.io...');

	const entries = Object.entries(archives);

	for (let i = 0; i < entries.length; i++) {
		const [platform, file] = entries[i];
		if (!file) {
			continue;
		}

		const command = 'butler';
		const args = [
			'push',
			`"${file}"`,
			`"${options['itch-user']}/${options['itch-channel']}:against-the-tide-${platform}"`,
			'--userversion',
			`${version}`,
		];

		console.log(`\n${platform}...`);
		console.log(`Deploy Command: ${command} ${args.join(' ')}`);

		await new Promise((resolve, reject) => {
			spawn(command, args, { stdio: 'inherit', shell: true }).on('close', (code) =>
				code == 0 ? resolve(null) : reject(new Error(`Unknown error code ${code}`)),
			);
		});
	}
}

// -----------------------------------------------------------------------------

function throwIf(truthy, message) {
	if (truthy != false) {
		throw new Error(message);
	}
}

// -----------------------------------------------------------------------------

let alreadyCleanedUp = false;

function userExit() {
	if (alreadyCleanedUp) {
		return;
	}

	console.log('Caught exit...');
	cleanup();
	process.exit(1);
}

function uncaughtException() {
	if (alreadyCleanedUp) {
		return;
	}

	console.log('Something went wrong...');
	cleanup();
	process.exit(1);
}

function cleanup() {
	alreadyCleanedUp = true;

	console.log(
		execSync('taskkill.exe /t /f /fi "imagename eq unity.exe" /fi "status eq running"', {
			encoding: 'utf-8',
		}),
	);

	if (originalProjectSettings) {
		writeFileSync(projectSettingsPath, originalProjectSettings, { encoding: 'utf8' });
	}
}

process.on('SIGINT', userExit);
process.on('SIGTERM', userExit);
process.on('exit', (code) => (code ? uncaughtException() : undefined));
