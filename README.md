# Unity Builder & itch.io Deployer

## Prerequisites

-   [node](https://nodejs.org)
-   [butler](https://itch.io/docs/butler/installing.html)

## Installation

-   Download git repo into a folder on your computer
-   Open a terminal
    -   Windows
        -   [Terminal](https://devblogs.microsoft.com/commandline/introducing-windows-terminal)
        -   [Powershell](https://helpdeskgeek.com/free-tools-review/using-powershell-for-home-users-a-beginners-guide/)
        -   [CommandPrompt](https://www.lifewire.com/command-prompt-2625840)
        -   [Git Bash](https://appuals.com/what-is-git-bash/)
    -   [OSX](https://www.macworld.co.uk/how-to/how-use-terminal-on-mac-3608274)
    -   linux
        -   You know what you're doing
-   `butler login`
-   `cd path/to/folder/you/create`
    -   Or the windows version `dir`
-   `npm install`
-   `npm start -- [flags]`
    -   The `--` is necessary

By default, the script will attempt to build windows, OSX, and linux. You can configure this to your needs with the relevant flags.

Keep in mind, all paths are relative to the folder you call the builder from. So absolute paths are safer than relative ones.

## Flags

-   `--project [string]`
    -   The path to your game's folder. Same rules as any other path. Double quotes and double slashes as necessary.
-   `--build [string]`
    -   The path where Unity will build your game. `./Build` would put it in whatever folder you called the builder script.
    -   The builder will append the OS name (windows, etc) to this path.
-   `--filename [string]`
    -   Filename of the game executable created by Unity.
    -   Surround with double quotes if you have spaces in your name:
        -   `--filename "My Game"`
-   `--archive [string]` _(**required** if `--no-archive` is **not** set)_
    -   Base filename of the archives to be created
    -   It will have the OS and version appended:
        -   `--archive my-game-archive` becomes `my-game-archive-windows-0.5.0-10.zip`
-   `--unity [string]`
    -   Path to the unity folder on your computer
        -   e.g., `C:\Program Files\Unity\Editor`
    -   If there is a space anywhere in your path, surround it with double quotes `"C:\Program Files\etc"`
    -   On windows, use double slashes for folder separator:
        -   `C:\\\Program Files\\\etc"`
-   `--unity-exe [string]`
    -   Filename of unity executable.
    -   Defaults to `unity.exe`
-   `--config [string]` _(optional)_
    -   Path to the builder config file. A JSON file containing the build number of your game.
    -   This is a custom file so if you want to include build number, creating the file with this content: `{build_number: 0}`
    -   Note: this builder doesn't increment the build number. That's better done [elsewhere](https://againstthetide.itch.io/game/devlog/206087/incrementing-a-build-number-in-unity-automatically).
-   `--env [string]` (otional)
    -   Specified an environment such as `development` or `production`. Used with `--env-source` and `-env-out` to read and write an environment config file used by your game.
    -   This "environment config file" is a JSON file that should include per-environment information your game uses depending on its environment. `test` might contact X server while `production` contacts Y server. Or whatever other settings you might use for your game that changes based on which environment you deploy.
-   `--env-source [string]` _(**required** by `--env`)_
    -   Specifies the path of folder with the file `environment.ENV.json` where ENV is whatever you set in `--env`.
-   `--env-out [string]` _(**required** by `--env`)_
    -   The path of the folder to write a copy of the JSON file found by `--env-source`. For example, `Assets/Resources`?
-   `--deploy` _(optional)_
    -   Attempts to upload compiled builds to itch.io
-   `--itch-user [string]` _(**required** by `--deploy`)_
    -   The account username that owns your game on itch.io. Your user in the following `http://myaccount.itch.io/mygame` would be `myaccount`.
-   `--itch-channel [string]` _(**required** by `--deploy`)_
    -   The "channel" of you game, aka the url name. Your channel in the following
-   `--major`
    -   Not an actual option. Incrementing the major (0.x.x) is important enough for you to do it by hand within Unity. Most projects never go from 0.x.x to 1.x.x anyway.
-   `--minor [number]` _(optional)_
    -   Increments the minor part of the version number (x.0.x)
-   `--patch [number]` _(optional)_
    -   Increment the patch part of the version number (x.x.0)
-   `--no-windows` _(optional)_
    -   Skips windows build
-   `--no-osx` _(optional)_
    -   Skips OSX build
-   `--no-linux` _(optional)_
    -   Skips linux build
-   `--web` _(optional)_
    -   Compiles web build
    -   This should be set if you haven't written a custom class for deploying web. Compiling for web is a ["by-hand" process](https://stackoverflow.com/questions/45081576/how-can-i-compile-a-unity3d-game-project-from-command-line-into-webgl) currently.
-   `--no-archive` _(optional)_
    -   Skips compressing builds to zip files
    -   Not necessary to deploy to itch.io, but it does sometimes make it easier for itch to determine smaller patches.`http://myaccount.itch.io/mygame` would be `mygame`.

## Warnings

This script auto-updates your application version by directly writing to the ProjectSettings file. I've never had a problem with it, but should something go wrong it could break your game. You have options:

-   Don't use this script
-   Don't use the auto-increment flags
-   Make a copy of the ProjectSettings file
-   Use version control (which you should anyway)
